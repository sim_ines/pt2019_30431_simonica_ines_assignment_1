package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import controller.ControllerMonom;
import model.Monom;
//JUnit tests on Monomials
class ControllerMonomTest {

	@Test
	void testInmultireMonoame() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(2, 2);
		Monom m3 = new Monom(10, 5);
		ControllerMonom c = new ControllerMonom();
		assert m3.equals(c.inmultireMonoame(m1, m2));	
	}

	@Test
	void testDerivareMonom() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(15, 2);
		ControllerMonom c = new ControllerMonom();
		assert m2.equals(c.derivareMonom(m1));
	}

	@Test
	void testIntegrareMonom() {
		Monom m1 = new Monom(4, 3);
		Monom m2 = new Monom(1, 4);
		ControllerMonom c = new ControllerMonom();
		assert m2.equals(c.integrareMonom(m1));
	}

	@Test
	void testEquals() {
		Monom m1 = new Monom(4, 3);
		Object m2 = new Monom(4, 3);
		ControllerMonom c = new ControllerMonom();
		assert m1.equals(m2);
	}

}
