package test;

import org.junit.jupiter.api.Test;

import controller.ControllerPolinom;
import model.Monom;
import model.Polinom;
//JUnit tests on Polynomials
class ControllerPolinomTest {

	@Test
	void testAdunarePolinoame() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(15, 2);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		p1.insertMonom(m2);
		
		Monom m3 = new Monom(2,2);
		Monom m4 = new Monom(1,3);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		Monom m5 = new Monom(6,3);
		Monom m6 = new Monom(17,2);
		Polinom p3 = new Polinom();
		p3.insertMonom(m5);
		p3.insertMonom(m6);
		ControllerPolinom c = new ControllerPolinom();
		assert p3.equals(c.adunarePolinoame(p1, p2));
	}

	@Test
	void testScaderePolinoame() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(15, 2);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		p1.insertMonom(m2);
		
		Monom m3 = new Monom(2,2);
		Monom m4 = new Monom(1,3);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		Monom m5 = new Monom(4,3);
		Monom m6 = new Monom(13,2);
		Polinom p3 = new Polinom();
		p3.insertMonom(m5);
		p3.insertMonom(m6);
		ControllerPolinom c = new ControllerPolinom();
		assert p3.equals(c.scaderePolinoame(p1, p2));
	}

	@Test
	void testInmultirePolinoame() {
		Monom m1 = new Monom(1, 3);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		
		Monom m3 = new Monom(2,2);
		Monom m4 = new Monom(2,3);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		Monom m5 = new Monom(2,5);
		Monom m6 = new Monom(2,6);
		Polinom p3 = new Polinom();
		p3.insertMonom(m5);
		p3.insertMonom(m6);
		ControllerPolinom c = new ControllerPolinom();
		assert p3.equals(c.inmultirePolinoame(p1, p2));
	}

	@Test
	void testDerivarePolinom() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(3, 2);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		p1.insertMonom(m2);
		
		Monom m3 = new Monom(15,2);
		Monom m4 = new Monom(6,1);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		ControllerPolinom c = new ControllerPolinom();
		assert p2.equals(c.derivarePolinom(p1));
	}

	@Test
	void testIntegrarePolinom() {
		Monom m1 = new Monom(8, 3);
		Monom m2 = new Monom(3, 2);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		p1.insertMonom(m2);
		
		Monom m3 = new Monom(2,4);
		Monom m4 = new Monom(1,3);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		ControllerPolinom c = new ControllerPolinom();
		assert p2.equals(c.integrarePolinom(p1));
	}

	@Test
	void testEquals() {
		Monom m1 = new Monom(5, 3);
		Monom m2 = new Monom(5, 2);
		Polinom p1 = new Polinom();
		p1.insertMonom(m1);
		p1.insertMonom(m2);
		
		Monom m3 = new Monom(5,2);
		Monom m4 = new Monom(5,3);
		Polinom p2 = new Polinom();
		p2.insertMonom(m3);
		p2.insertMonom(m4);
		
		assert p2.equals(p1);
	}
	
	@Test
	void testImpartirePolinoame() {
		Monom m1 = new Monom(3, 2);
		Monom m2 = new Monom(5, 1);
		Monom m3 = new Monom(2, 0);

		Polinom p1 = new Polinom();
		p1.insertMonom(m1);	p1.insertMonom(m2);	p1.insertMonom(m3);

		Monom m4 = new Monom(2,1);
		Monom m5 = new Monom(1,0);
		Polinom p2 = new Polinom();
		p2.insertMonom(m4);	p2.insertMonom(m5);
		
		Monom m6 = new Monom(3.0/2.0, 1);
		Monom m7 = new Monom(7.0/4.0, 0);
		Monom m8 = new Monom(1.0/4.0, 0);//Remainder
		Polinom p3 = new Polinom();
		Polinom p4 = new Polinom();
		p3.insertMonom(m6);	p3.insertMonom(m7);
		p4.insertMonom(m8);//remainder
		
		p3.cleanPolinom(); p4.cleanPolinom();
		ControllerPolinom c = new ControllerPolinom();
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println(p3.toString());
		System.out.println(p4.toString());
		
		assert p3.equals(c.impartirePolinoame(p1, p2)[0]) && p4.equals(c.impartirePolinoame(p1, p2)[1]);
	}
}
