package controller;

import model.Monom;
//ControllerMonom: operations done with monomials
public class ControllerMonom {

    public ControllerMonom(){

    }


    public Monom sumaMonoame(Monom m1, Monom m2){
        Monom m3 = new Monom(m1.getCoef()+m2.getCoef(), m1.getGrad());
        return m3;
    }

    public Monom scadereMonoame(Monom m1, Monom m2){
        Monom m3 = new Monom(m1.getCoef()-m2.getCoef(), m1.getGrad());
        return m3;
    }

    public Monom inmultireMonoame(Monom m1, Monom m2){
        Monom m3 = new Monom (m1.getCoef()*m2.getCoef(), m1.getGrad()+m2.getGrad());
        return m3;
    }
    
    public Monom impartireMonoame(Monom m1, Monom m2) {
    	Monom m3 = new Monom (m1.getCoef()/m2.getCoef(), m1.getGrad()-m2.getGrad());
        return m3;
    }

    public Monom derivareMonom(Monom m1){

        Monom m3 = new Monom(m1.getCoef()*m1.getGrad(), m1.getGrad()-1);
        return m3;
    }

    public  Monom integrareMonom(Monom m1){
        Monom m3 = new Monom(m1.getCoef()/(m1.getGrad()+1), m1.getGrad()+1);
        return m3;
    }
}
