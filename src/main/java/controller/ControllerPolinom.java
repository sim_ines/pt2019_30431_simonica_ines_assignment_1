package controller;

import model.Monom;
import model.Polinom;
import view.Gui;
//ControllerPolinom: operations done with polynomials
public class ControllerPolinom {

    public Polinom adunarePolinoame(Polinom p1, Polinom p2){
        Polinom p3 = new Polinom();
        for(Monom m: p1.getPoli()){
        	Monom newMonom = new Monom(m.getCoef(), m.getGrad());
            p3.insertMonom(newMonom);
        }
        for(Monom m: p2.getPoli()){
        	Monom newMonom = new Monom(m.getCoef(), m.getGrad());
            p3.insertMonom(newMonom);
        }
        p3.cleanPolinom();
        p3.sortPolinom();
        return p3;
    }

    public Polinom scaderePolinoame(Polinom p1, Polinom p2){
        Polinom p3 = new Polinom();
        for(Monom m: p1.getPoli()){
        	Monom newMonom = new Monom(m.getCoef(), m.getGrad());
            p3.insertMonom(newMonom);
        }
        for(Monom m: p2.getPoli()){
            Monom monomScazut = new Monom(m.getCoef()*(-1), m.getGrad());
            p3.insertMonom(monomScazut);
        }
        p3.cleanPolinom();
        p3.sortPolinom();
        return p3;
    }

    public Polinom inmultirePolinoame(Polinom p1, Polinom p2){
        Polinom p3 = new Polinom();
        for(Monom m1: p1.getPoli()){
            for(Monom m2: p2.getPoli()){
                ControllerMonom c = new ControllerMonom();
                Monom m3 = c.inmultireMonoame(m1,m2);
                p3.insertMonom(m3);
            }
        }
        p3.cleanPolinom();
        p3.sortPolinom();
        return p3;
    }

    public Polinom[] impartirePolinoame(Polinom p1, Polinom p2) {
    	Polinom cat = new Polinom();
    	Polinom rest = new Polinom();
    	Polinom copiep1 = new Polinom();
    	Polinom[] result = new Polinom[2];
    	copiep1 = p1;
    	Monom m1 = p1.getGradH(); Monom m2 = p2.getGradH();
    	if(m1.getGrad()<m2.getGrad()) {
    		cat.insertMonom(new Monom(0,0));
    		rest = p1;
    	}
    	else {
	    	while(m1.getGrad()>=m2.getGrad()) {
	    		ControllerMonom c = new ControllerMonom() ;
	    		Monom m3 = c.impartireMonoame(m1, m2);
	    		cat.insertMonom(m3);
	    		ControllerPolinom c1 = new ControllerPolinom();
	    		Polinom pAux = new Polinom();
	    		pAux.insertMonom(m3);
	    		copiep1 = c1.scaderePolinoame(copiep1, c1.inmultirePolinoame(p2, pAux));
	    		copiep1.cleanPolinom();
	    		System.out.println(copiep1.toString());
	    		m1.setGrad(copiep1.getGradH().getGrad());
	    		m1.setCoef(copiep1.getGradH().getCoef());
	    	}
	    	if(m1.getGrad() == 0 && m1.getCoef() == 0) rest.insertMonom(new Monom(0,0));
	    	else rest= copiep1;
	    }
    	result[0] = cat; result[1] = rest;
    	return result;	
    }
    
    public Polinom derivarePolinom(Polinom p1){
        Polinom p2 = new Polinom();
        for(Monom m1 : p1.getPoli()){
            ControllerMonom c = new ControllerMonom();
            Monom m2 = c.derivareMonom(m1);
            p2.insertMonom(m2);
        }
        p2.cleanPolinom();
        p2.sortPolinom();
        return p2;
    }

    public Polinom integrarePolinom(Polinom p1){
        Polinom p2 = new Polinom();
        for(Monom m1 : p1.getPoli()){
            ControllerMonom c = new ControllerMonom();
            Monom m2 = c.integrareMonom(m1);
            p2.insertMonom(m2);
        }
        p2.cleanPolinom();
        p2.sortPolinom();
        return p2;
    }





    public static void main(String[] args){
    		Gui g = new Gui();
    }


}
