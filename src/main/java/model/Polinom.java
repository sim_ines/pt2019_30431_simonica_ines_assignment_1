package model;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Polinom class: each polynomial is a list of monomials. Has operations done on Polynomials	
public class Polinom {

    private List<Monom> poli;

    public Polinom(){
        this.poli = new ArrayList<Monom>();
    }

    public Polinom(String s){
        this.poli = new ArrayList<Monom>();
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)"); //REGGEX pattern identification
        Matcher matcher = pattern.matcher(s); //pattern matcher e.g. 3x^2 

        int x=0;
        while (matcher.find()) { //till there is still something to find
        	
            x=x+1;
            Monom m = new Monom(matcher.group(1)); //collecting the identified monomial in m
            poli.add(m); //add it to the polynomial.
        }
    }
    
    public List<Monom> getPoli(){
        return this.poli;
    }
    
    public void setPoli(List<Monom> l) {
    	if(!poli.isEmpty())
    		this.poli.clear();
    	for(Monom m: l) {
    		this.insertMonom(new Monom(m.getCoef(), m.getGrad()));
    	}
    }

    public void insertMonom(Monom m){
        this.poli.add(m);
    }

    public void sortPolinom(){
       this.poli.sort(new Comparator<Monom>() { //sorting the polynomial list. new criteria: by monomials.
           public int compare(Monom o1, Monom o2) {
               if(o1.getGrad()< o2.getGrad())
                   return 1;
               else
                   return -1;
           }
       });
    }

    public void cleanPolinom(){ //eliminating the monomials with the same grade by merging them.
        List<Monom> q = new ArrayList<Monom>();
        for(Monom m : poli){
            if(m.getCoef() != 0) {
                if (!q.isEmpty()) {
                    boolean monomAdaugat = false;
                    for (Monom p : q) {
                        if (m.getGrad() == p.getGrad()) {
                            p.setCoef(p.getCoef() + m.getCoef());
                            monomAdaugat = true;
                        }

                    }
                    if (!monomAdaugat) {
                        q.add(m);
                    }
                } else {
                    q.add(m);
                }
            }
        }
        poli= q;
    }

    public void printPolinom(){
        System.out.println("Polinom:");
        for(Monom m : poli){
            m.printMonom();
            System.out.println();

        }
    }


    @Override
    public String toString() { //printing the polynomial 
        String s = new String();
        for(Monom m : poli) {
        	if(m.getCoef()>=0)
        		s += "+" + m.getCoef() + "X^" + m.getGrad();
        	else
        		s += m.getCoef() + "X^" + m.getGrad();        		
        }
            return s;
    }
    

    public Monom getGradH() { //get the monomial with the highest grade
    	if(this.poli.size() > 0) {
	    	this.sortPolinom();
	    	return new Monom(this.poli.get(0).getCoef(), this.poli.get(0).getGrad());
    	}
    	else
    		return new Monom(0,0);
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(!(obj instanceof Polinom)) {
    		return false;
    	}
    	else {
    		Polinom p1 = new Polinom(); Polinom p2 = new Polinom();   		
    		for(Monom m: this.poli) { 
    			Monom monomcopie = new Monom(m.getCoef(), m.getGrad());
    			p1.insertMonom(monomcopie);
    		}
    		for(Monom m: ((Polinom)obj).poli) {
    			Monom monomcopie = new Monom(m.getCoef(), m.getGrad());
    			p2.insertMonom(monomcopie);
    		}
    		p1.cleanPolinom();	p2.cleanPolinom();
    		p1.sortPolinom();	p2.sortPolinom();
    		for(Monom m: p1.poli) {
    			if(!p2.poli.contains(m)) {
    				return false;
    			}
    		}
    		for(Monom m: p2.poli) {
    			if(!p1.poli.contains(m)) {
    				return false;
    			}
    		}
    		return true;
    	}
    }
}
