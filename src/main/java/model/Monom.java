package model;
//Monom class: operations done on monomials
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom {
    private double coef;
    private int grad;

    public Monom(double coef, int grad){
        this.coef = coef;
        this.grad = grad;
    }

    public Monom(String s){
        Pattern pattern = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher matcher = pattern.matcher(s);

        int x=0;
        while (matcher.find()) {
            x=x+1;
            this.coef = Double.parseDouble(matcher.group(1));
            this.grad = Integer.parseInt(matcher.group(2));

            //System.out.println("Group "+x+": " + matcher.group(2));
        }
    }
    public double getCoef(){
        return this.coef;
    }

    public int getGrad(){
        return this.grad;
    }

    public void setCoef(double coef){
        this.coef = coef;
    }

    public void setGrad(int grad){
        this.grad = grad;
    }

    public void printMonom(){
        System.out.println("coef: "+ this.coef + " grad: "+ this.grad);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Monom){
            return this.coef == ((Monom) obj).coef && this.grad == ((Monom) obj).grad;
        }
        return false;
    }
}




