package view;
//graphical user interface
import controller.ControllerPolinom;
import model.Polinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Gui {

    JLabel rezultatAdunare;
    JLabel rezultatScadere;
    JLabel rezultatInmultire;
    JLabel rezultatCatImpartire;
    JLabel rezultatRestImpartire;
    JLabel rezultatDerivare;
    JLabel rezultatIntegrare;
    
    JLabel p;
    JTextField poli1;
    JLabel q;
    JTextField poli2;
    
    JButton button;
    JButton clear;
    
    public Gui(){ //all the GUI components

        rezultatAdunare = new JLabel();
        rezultatAdunare.setVisible(true);

        rezultatScadere = new JLabel();
        rezultatScadere.setVisible(true);

        rezultatInmultire = new JLabel();
        rezultatInmultire.setVisible(true);

        rezultatCatImpartire = new JLabel();
        rezultatCatImpartire.setVisible(true);

        rezultatRestImpartire = new JLabel();
        rezultatRestImpartire.setVisible(true);

        rezultatDerivare = new JLabel();
        rezultatDerivare.setVisible(true);

        rezultatIntegrare = new JLabel();
        rezultatIntegrare.setVisible(true);
    	
        p = new JLabel("P(x):");
        p.setVisible(true);
    	
        q = new JLabel("Q(x):");
        q .setVisible(true);
        
        button = new JButton("Compute");
        clear = new JButton("Clear");

        poli1 = new JTextField() ;
        poli1.setEditable(true);
        poli2 = new JTextField() ;
        poli2.setEditable(true);
        poli1.setVisible(true);
        poli2.setVisible(true);

        initElements();
                
        button.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) {
        		  if(!poli1.getText().isEmpty() && !poli2.getText().isEmpty()) {
        			  processGuyy();
        		  }		   
        	  } 
        });
    	
        clear.addActionListener(new ActionListener() { 
      	  public void actionPerformed(ActionEvent e) { 
      		  initElements();
      		  } 
      		});
    	
        JFrame window = new JFrame();
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS)); //boxlayout, gridbag layout, flow layout
        panel.add(p);
        panel.add(poli1);
        panel.add(q);
        panel.add(poli2);
        
        panel.add(clear);
        panel.add(button);
        
        panel.add(rezultatAdunare);
        panel.add(rezultatScadere);
        panel.add(rezultatInmultire);
        panel.add(rezultatCatImpartire);
        panel.add(rezultatRestImpartire);
        panel.add(rezultatDerivare);
        panel.add(rezultatIntegrare);
        
       
        panel.setVisible(true);

        
        window.setBounds(0,0,300,300);


       // window.add(frame);
        window.add(panel);
        window.setVisible(true);

    }

    private void initElements() {
    		rezultatAdunare.setText("P(x)+Q(x): ");
	  		rezultatScadere.setText("P(x)-Q(x): ");
	  		rezultatInmultire.setText("P(x)*Q(x): ");
	  		rezultatCatImpartire.setText("P(x)/Q(x): ");
	  		rezultatRestImpartire.setText("P(x)%Q(x): ");
	  		rezultatDerivare.setText("P(x)': ");
	  		rezultatIntegrare.setText("P(x) Integrated: ");
	  		poli1.setText("");
	  		poli2.setText("");
    }
    
    private void processGuyy(){
        Polinom p = new Polinom(poli1.getText());
        Polinom q = new Polinom(poli2.getText());

        ControllerPolinom cp = new ControllerPolinom();
        //setting each field to the corresponding result.
        Polinom rezultatAdunareLocal = cp.adunarePolinoame(p,q);
        rezultatAdunare.setText(rezultatAdunare.getText() + rezultatAdunareLocal.toString());
        
        Polinom rezultatScadereLocal = cp.scaderePolinoame(p, q);
        rezultatScadere.setText(rezultatScadere.getText() + rezultatScadereLocal.toString());
        
        Polinom rezultatInmultireLocal = cp.inmultirePolinoame(p, q);
        rezultatInmultire.setText(rezultatInmultire.getText() + rezultatInmultireLocal.toString());
        
        Polinom rezultatCatImpartireLocal = cp.impartirePolinoame(p, q)[0];
        rezultatCatImpartire.setText(rezultatCatImpartire.getText()+ rezultatCatImpartireLocal.toString());
        Polinom rezultatRestImpartireLocal = cp.impartirePolinoame(p, q)[1];
        rezultatRestImpartire.setText(rezultatRestImpartire.getText()+ rezultatRestImpartireLocal.toString());

        Polinom rezultatDerivareLocal = cp.derivarePolinom(p);
        rezultatDerivare.setText(rezultatDerivare.getText() + rezultatDerivareLocal.toString());
        
        Polinom rezultatIntegrareLocal = cp.integrarePolinom(p);
        rezultatIntegrare.setText(rezultatIntegrare.getText() + rezultatIntegrareLocal.toString());
    }





}
